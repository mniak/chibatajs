# chibatajs [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> A TimeTracker that integrates VSTS with Toggl

## Installation

```sh
$ npm install --save chibatajs
```

## Usage

```js
const chibatajs = require('chibatajs');

chibatajs('Rainbow');
```
## License

GPL-3.0 © [Andre Soares]()


[npm-image]: https://badge.fury.io/js/chibatajs.svg
[npm-url]: https://npmjs.org/package/chibatajs
[travis-image]: https://travis-ci.org/mniak/chibatajs.svg?branch=master
[travis-url]: https://travis-ci.org/mniak/chibatajs
[daviddm-image]: https://david-dm.org/mniak/chibatajs.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/mniak/chibatajs
[coveralls-image]: https://coveralls.io/repos/mniak/chibatajs/badge.svg
[coveralls-url]: https://coveralls.io/r/mniak/chibatajs
