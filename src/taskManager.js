import Task from './task';

// @flow

/**
 * The task manager is responsible for creating tasks
 */
class TaskManager {
  constructor(vstsManager, togglManager) {
    this._vstsManager = vstsManager;
    this._togglManager = togglManager;
  }

  async startNew(title, estimate) {
    const taskId = await this._vstsManager.createWorkItem(title, estimate / 3600.0);
    await this._vstsManager.activateWorkItem(taskId);

    await this._togglManager.startTimer(title, taskId);

    const task = new Task(this._vstsManager, this._togglManager, taskId, title, estimate, true);
    return task;
  }

}

module.exports = TaskManager;
