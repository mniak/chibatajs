// @flow

/**
 * A task
 */
class Task {
  /**
   * Creates a new Task with id, title and estimate
   * @param {VstsManager} vstsManager The VSTS manager
   * @param {TogglManager} togglManager The Toggl manager
   * @param {number} id The id of the task
   * @param {string} title The title of the task
   * @param {number} estimate The task estimate in seconds
   */
  constructor(vstsManager, togglManager, id, title, estimate, isActive) {
    this._vstsManager = vstsManager;
    this._togglManager = togglManager;
    this._id = id;
    this._title = title;
    this._estimate = estimate;

    if (isActive === true) {
      this._isActive = true;
      this._isNew = false;
    }
  }
  /**
   * Gets the task id
   */
  get id() {
    return this._id;
  }
  /**
   * Gets the task title
   */
  get title() {
    return this._title;
  }
  /**
   * Gets the task estimate in seconds
   */
  get estimate() {
    return this._estimate;
  }

  get isActive() {
    return this._isActive;
  }
  get isNew() {
    return this._isNew;
  }
  /**
   * Pauses the task
   */
  async pause() {
    this._vstsManager.deactivateWorkItem(this.id);
    this._togglManager.stopTimer(this.id);
  }
}
module.exports = Task
