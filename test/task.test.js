import Task from "../src/task";
import math from "mathjs";
import randomstring from "randomstring";

describe("Task", () => {
  describe("constructor", () => {
    test("with id should set id only", () => {
      const vstsManager = {};
      const togglManager = {};
      const id = math.randomInt(500);

      const task = new Task(vstsManager, togglManager, id);

      expect(task.id).toBe(id);
      expect(task.title).toBeUndefined();
      expect(task.estimate).toBeUndefined();
      expect(task.isStarted).toBeUndefined();
      expect(task.isRunning).toBeUndefined();
      expect(task.startDate).toBeUndefined();
    });

    test("with id and title should set id and title", () => {
      const vstsManager = {};
      const togglManager = {};
      const id = math.randomInt(500);
      const title = randomstring.generate(32);
      const estimate = math.randomInt(3600, 36000);

      const task = new Task(vstsManager, togglManager, id, title, estimate);

      expect(task.id).toBe(id);
      expect(task.title).toBe(title);
      expect(task.estimate).toBe(estimate);
      expect(task.isNew).toBeUndefined();
      expect(task.isActive).toBeUndefined();
    });
    test("with all params should all", () => {
      const vstsManager = {};
      const togglManager = {};
      const id = math.randomInt(500);
      const title = randomstring.generate(32);
      const estimate = math.randomInt(3600, 36000);
      const active = true;

      const task = new Task(vstsManager, togglManager, id, title, estimate, active);

      expect(task.id).toBe(id);
      expect(task.title).toBe(title);
      expect(task.estimate).toBe(estimate);
      expect(task.isNew).toBeFalsy();
      expect(task.isActive).toBeTruthy();
    });
  });

  //TODO: test fetch

  test("pause should call vsts and toggl", async () => {
    const vstsManager = {
      deactivateWorkItem: jest.fn(() => Promise.resolve()),
    };
    const togglManager = {
      stopTimer: jest.fn(() => Promise.resolve()),
    };
    const id = math.randomInt(500);
    const title = randomstring.generate(32);
    const estimate = math.randomInt(3600, 36000);

    const task = new Task(vstsManager, togglManager, id, title, estimate);
    await task.pause();

    expect(vstsManager.deactivateWorkItem).toBeCalled();
    expect(vstsManager.deactivateWorkItem).toBeCalledWith(id);

    expect(togglManager.stopTimer).toBeCalled();
    expect(togglManager.stopTimer).toBeCalledWith(id);
  });
});
