import TaskManager from "../src/taskManager";
import math from "mathjs";
import randomstring from "randomstring";

describe("TaskManager", () => {

  test("startNew should call vsts and toggl", async () => {
    const fakeTaskId = math.randomInt(500);

    const vstsManager = {
      createWorkItem: jest.fn(() => Promise.resolve(fakeTaskId)),
      activateWorkItem: jest.fn(() => Promise.resolve()),
    };
    const togglManager = {
      startTimer: jest.fn(() => Promise.resolve()),
    };
    const title = randomstring.generate(32);
    const estimate = math.randomInt(3600, 36000);

    const mgr = new TaskManager(vstsManager, togglManager);
    await mgr.startNew(title, estimate);

    expect(vstsManager.createWorkItem).toHaveBeenCalledTimes(1);
    expect(vstsManager.createWorkItem.mock.calls[0][0]).toBe(title);
    expect(vstsManager.createWorkItem.mock.calls[0][1]).toBeCloseTo(estimate / 3600.0, 5);

    expect(vstsManager.activateWorkItem).toHaveBeenCalled();
    expect(vstsManager.activateWorkItem).toHaveBeenCalledWith(fakeTaskId);

    expect(togglManager.startTimer).toHaveBeenCalled();
    expect(togglManager.startTimer).toHaveBeenCalledWith(title, fakeTaskId);
  });

  test("startNew should return a promise of task", async () => {
    const fakeTaskId = math.randomInt(500);

    const vstsManager = {
      createWorkItem: jest.fn(() => Promise.resolve(fakeTaskId)),
      activateWorkItem: jest.fn(() => Promise.resolve()),
    };
    const togglManager = {
      startTimer: jest.fn(() => Promise.resolve()),
    };
    const title = randomstring.generate(32);
    const estimate = math.randomInt(3600, 36000);

    const mgr = new TaskManager(vstsManager, togglManager);
    const task = await mgr.startNew(title, estimate);

    expect(task.id).toBe(fakeTaskId);
    expect(task.title).toBe(title);
    expect(task.estimate).toBe(estimate);
    expect(task.isNew).toBeFalsy();
    expect(task.isActive).toBeTruthy();
  });

});
